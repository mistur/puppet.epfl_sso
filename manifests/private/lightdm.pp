# Show manual login in latest ubuntu in case where the display manager is lightDM
#
class epfl_sso::private::lightdm {
  case $::osfamily {
    'Debian': {
      if ($::operatingsystemrelease in ['15.04', '15.10', '16.04', '16.10', '18.04'] and $::operatingsystem == 'Ubuntu') {
        file { "/etc/lightdm/lightdm.conf.d" :
          ensure => directory
        }
        file { "/etc/lightdm/lightdm.conf.d/50-show-manual-login.conf" :
          content => inline_template("#
# Managed by Puppet, (modules/epfl_sso/manifests/private/lightdm.pp)
# DO NOT EDIT
# See https://askubuntu.com/a/451980/68896
[Seat:*]
greeter-show-manual-login=true
")
        } ~> Service["lightdm"]

        ensure_resource("service", "lightdm", { ensure => "running" })
      } else {
        notify {"Enabling the manual greeter on version $::operatingsystemrelease of Ubuntu is not supported. Please check https://gitlab.com/epfl-sti/puppet.epfl_sso":}
      }
    }
  }
}
