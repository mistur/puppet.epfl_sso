class epfl_sso::private::smb_conf(
  $smb_conf_file = $epfl_sso::private::params::smb_conf_file,
  $smb_workgroup = $epfl_sso::private::params::smb_workgroup,
  $smb_realm     = $epfl_sso::private::params::krb5_domain
) inherits epfl_sso::private::params {
  ini_setting { "[global] workgroup":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'workgroup',
    value   => $smb_workgroup
  }

  ini_setting { "[global] realm":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'realm',
    value   => $smb_realm
  }

  ini_setting { "[global] security":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'security',
    value   => 'ads'
  }
}
