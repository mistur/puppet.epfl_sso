class epfl_sso::private::params {
  $smb_workgroup = "INTRANET"
  $krb5_domain = "INTRANET.EPFL.CH"
  if ("${::epfl_krb5_resolved}" != "true") {
    fail("Couldn't resolve the Kerberos domain controller for ${krb5_domain} - Something might be wrong with your DNS server settings")
  }
  $ad_server = "ad3.intranet.epfl.ch"
  $realm = "intranet.epfl.ch"

  $is_puppet_apply = !(defined('$::servername') and $::servername)

  case "${::operatingsystem} ${::operatingsystemrelease}" {
         'Ubuntu 12.04': {
           $sssd_packages = ['sssd']
           $needs_nscd = true
         }
         default: {
           $sssd_packages = ['sssd', 'sssd-ldap']
           $needs_nscd = false
         }
  }

  # The files that should be deleted upon restarting sssd
  $sssd_cleanup_globs = "/var/lib/sss/db/*"

  case $::osfamily {
    'Debian': {
      $pam_modules_managed_by_distro = ["krb5", "mkhomedir", "sss", "winbind" ]
    }
  }

  $krb5_conf_file = $::osfamily ? {
    "Darwin" => "/private/etc/krb5.conf",
    default  => "/etc/krb5.conf"
  }

  $is_dhcp = ($::networking and $::networking[dhcp])

  if (! $is_dhcp) {
    $ensure_gssapi_server = "fixed-ip"
  } elsif ($::domain == "intranet.epfl.ch") {
    $ensure_gssapi_server = "dhcp"
  } else {
    $ensure_gssapi_server = undef
  }

  $smb_conf_file = "/etc/samba/smb.conf"
  $manage_smb_conf = any2bool($::has_smb_conf)
  $manage_samba_secrets = any2bool($::has_smb_conf)
  $check_net_ads_testjoin = $manage_samba_secrets

  # In an email dated Sep 13, 2018, Laurent Durrer indicated that ad4 is not
  # part of the production cluster.
  $ad_servers_unqualified = ['ad1', 'ad2', 'ad3', 'ad5', 'ad6']
  $ad_server_urls_for_sssd = inline_template('<%= @ad_servers_unqualified.map { |h| "ldap://#{h}/" }. join(",") %>')
  $ad_server_ldaps_urls = inline_template('<%= @ad_servers_unqualified.map { |h| "ldaps://#{h}:3269/" }. join(" ") %>')

  $ad_server_base_dn = "DC=intranet,DC=epfl,DC=ch"

  $defaults_nfs_common_path = "/etc/default/nfs-common"

  case $::osfamily {
    "Debian": {
      $rpc_gssd_package = "nfs-common"
      $request_key_path = "/sbin/request-key"
      $nfsidmap_path = "/usr/sbin/nfsidmap"
      $request_key_package = "keyutils"
      $nfsidmap_package = "nfs-common"
      $ldap_conf_path = "/etc/ldap/ldap.conf"
    }
    "RedHat": {
      $rpc_gssd_package = "nfs-utils"
      $request_key_path = "/usr/sbin/request-key"
      $nfsidmap_path = "/usr/sbin/nfsidmap"
      $request_key_package = "keyutils"
      $nfsidmap_package = "nfs-utils"
      $ldap_conf_path = "/etc/openldap/ldap.conf"
    }
  }

  case $::osfamily {
    "Debian": {
      $autofs_deps = ["autofs", "autofs-ldap"]
      $autofs_service = "autofs"
    }
    "RedHat": {
      $autofs_deps = ["autofs"]
      $autofs_service = "autofs"
    }
  }

  $autofs_conf_path = "/etc/autofs.conf"
  $autofs_ldap_auth_conf_path = "/etc/autofs_ldap_auth.conf"

  case $::osfamily {
    "RedHat": {
      $sshd_service = "sshd"
    }
    "Debian": {
      $sshd_service = "ssh"
    }
  }

  # Timeout for in-kernel Kerberos credentials (derived from your
  # /tmp/krb5cc_123456_something by rpc.gssd upon kernel request
  # aka "upcall").
  #
  # Since there is no way to invalidate these (see sections 6 and 7 of
  # http://www.citi.umich.edu/projects/nfsv4/linux/faq/), better put a
  # *very* conservative timeout on them - basically as low as we can
  # without hurting performance.
  $kernel_gss_lifetime_secs = 30
}
