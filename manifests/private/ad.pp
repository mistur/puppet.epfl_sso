# Class: epfl_sso::private::ad
#
# Integrate this computer into EPFL's Active Directory
#
# This class is the translation into Puppet of
# https://fuhm.net/linux-and-active-directory/
#
# Unlike Windows, this approach does *not* preclude cloning - A number
# of VMs can share the same Kerberos credentials with no issues,
# provided $renew_domain_credentials is set to false (or
# alternatively, all clones should have a different hostname)
#
# === Parameters:
#
# $join_domain:: Whether to maintain presence in the Active Directory domain.
#                Puppet will *not* attempt to join the domain proper, as that
#                requires administrative credentials and cannot be done in
#                batch. Instead, an error message will be shown telling you
#                what to do (basically, kinit + use an epfl_sso-provided shell
#                script that drives msktutil(1) et al).
#
# $manage_ldap_conf::  Whether to set the Active Directory servers as
#                      the default LDAP servers in ldap.conf
#
# $manage_samba_secrets::  Whether to pass --set-samba-secret to msktutil.
#                          Has no effect absent a working, properly configured
#                          Samba installation.
#
# $renew_domain_credentials:: Whether to periodically renew the
#                Kerberos keytab entry. Has no effect under "puppet
#                agent"; RECOMMENDED for "puppet apply" unless this
#                machine is a clonable master that shares the same
#                host name with a number of clones.
#
# $ad_server::   The Active Directory server to use
#
# $realm::       The Active Directory realm to use
#
# $epflca_cert_url:: Where to find the certificate for the EPFL CA
#                (necessary for ldapsearch to work)
#
# === Actions:
#
# * Create EPFL-compatible /etc/krb5.conf
#
# * Deploy pam_krb5.so in an "opportunistic" configuration: grab a TGT
#   if we can, but fail gracefully otherwise
#
# * Entrust the EPFL-CA with OpenLDAP clients
#
# * Add suitable entries to /etc/hosts for ad{1,2,3}.{intranet.,}epfl.ch
#
# * Optionally (depending on $join_domain), create or update Active
#   Directory-compatible credentials in /etc/krb5.keytab . Note that cloning
#   virtual machines that are registered in the domain suffers from the same
#   kind of issues as on the Windows platform; as each VM instance will try
#   to update the Kerberos password for the AD entry, they will quickly diverge
#   since only one of them will succeed to do so.
#
# * If running "puppet apply", and if both $join_domain and
#   $renew_domain_credentials are true, set up a crontab to renew the
#   keytab daily

class epfl_sso::private::ad(
  $ad_server,
  $realm,
  $join_domain,
  $manage_ldap_conf = true,
  $epflca_cert_url = 'https://certauth.epfl.ch/epflca.cer',
  $renew_domain_credentials = true,
  $manage_samba_secrets = $::epfl_sso::private::params::manage_samba_secrets,
  $check_net_ads_testjoin = $::epfl_sso::private::params::check_net_ads_testjoin,
) inherits epfl_sso::private::params {
  # Kerberos servers who would like to identify their peer using a
  # reverse DNS are in for a surprise for some of the hosts... Among
  # which, the AD servers themselves :(
  define etchosts_line($ip) {
    host { "${title}.${::epfl_sso::private::ad::realm}":
      host_aliases => $title,
      ip => $ip,
      ensure => "present"
    }
  }

  epfl_sso::private::ad::etchosts_line { "ad1": ip => "128.178.15.227" }
  epfl_sso::private::ad::etchosts_line { "ad2": ip => "128.178.15.228" }
  epfl_sso::private::ad::etchosts_line { "ad3": ip => "128.178.15.229" }
  # In an email dated Sep 13, 2018, Laurent Durrer indicated that ad4 is not
  # part of the production cluster.
  epfl_sso::private::ad::etchosts_line { "ad5": ip => "128.178.15.231" }
  epfl_sso::private::ad::etchosts_line { "ad6": ip => "128.178.15.232" }

  class { "epfl_sso::private::ldap":
    manage_ldap_conf => $manage_ldap_conf,
  }
  epfl_sso::private::ldap::trusted_ca_cert { 'epfl':
    url => $epflca_cert_url,
    ensure => 'present'
  }

  class { "epfl_sso::private::gssapi": }

  case $::kernel {
    'Darwin': {
      notice("Mac OS X detected - Skip setup of Active Directory users, groups and authentication")
    }
    'Linux': {
      case $::osfamily {
        "Debian": {
          ensure_packages([ "krb5-user", "libpam-krb5"])
        }
        "RedHat": {
          # https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Managing_Smart_Cards/installing-kerberos.html
          ensure_packages(["krb5-workstation", "krb5-libs", "pam_krb5"])
        }
        default: {
          fail("Not sure how to install Kerberos client-side support on ${::osfamily}-family Linux")
        }
      }

      if ($join_domain) {
        ensure_packages("msktutil")

        if ($manage_samba_secrets) {
          case $::osfamily {
            "Debian": {
              ensure_packages(["tdb-tools", "samba-common-bin"])
            }
            "RedHat": {
              ensure_packages(["tdb-tools", "samba-common-tools"])
            }
            default: {
              fail("Not sure how to install \"net\", \"tdbdump\" and \"tdbtool\" on ${::osfamily}-family Linux")
            }
          }
        }

        $_adjoin_path = "/usr/local/sbin/adjoin"
        file { $_adjoin_path:
          content => template("epfl_sso/adjoin.erb"),
          mode    => "0755"
        } ->
        exec { "${_adjoin_path} update status":
          path => $::path,
          unless => "${_adjoin_path} update status",
          require => [
            Anchor["epfl_sso::samba_configured"],
            Package["msktutil"]
          ]
        } ->
        anchor { "epfl_sso::ad_joined": }

        if ($renew_domain_credentials and
            $epfl_sso::private::params::is_puppet_apply) {
              package { "moreutils":
                ensure => "installed"  # For the chronic command
              } ->
              file { "/etc/cron.daily/renew-AD-credentials":
                mode => "0755",
                content => inline_template("#!/bin/sh
# Renew keytab, lest Active Directory forget about us after 90 days
#
# Managed by Puppet, DO NOT EDIT

chronic <%= @_adjoin_path %> update status
")
              }
        }
      }   # if ($join_domain)
    }  # case $::kernel is 'Linux'
    default: {
      fail("Unsupported operating system: ${::kernel}")
    }
  }  # case $::kernel
}    # class epfl_sso::private::ad
