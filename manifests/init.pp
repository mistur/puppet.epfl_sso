# coding: utf-8
# Class: epfl_sso
#
# This class describes integrating a Linux computer into the EPFL
# directory services (LDAP and Kerberos)
#
# === Parameters:
#
# $allowed_users_and_groups::  access.conf(5)-style ACL, e.g.
#                              "user1 user2 (group1) (group2)"
#
# $manage_nsswitch_netgroup::  Whether to manage the netgroup entry in
#                              nsswitch.conf
#
# $enable_mkhomedir::          Whether to automatically create users' home
#                              directories upon first login
#
# $ad_automount_home::         Whether to use the auto.home automount map
#                              from Active Directory. Requires
#                              $directory_source == "AD"; mutually exclusive
#                              with $enable_mkhomedir. Set this to false
#                              either if the homes are to be local, or the
#                              automount map is managed by some other
#                              mechanism.
#
# $needs_nscd::                Whether to install nscd to serve as a second
#                              layer of cache (for old distros with slow sssd)
#
# $ad_server::                 The multi-homed DNS name of the Active Directory server
#
# $ad_server_base_dn::         The Base DN of the Active Directory LDAP tree
#
# $auth_source::               Either "AD" or "scoldap"
#
# $directory_source::          Either "AD" or "scoldap"
#
# $join_domain::               True to check that we are joined to the domain.
#                              Also the default value for
#                              $renew_domain_credentials below. NOTE: This
#                              *WILL NOT* auto-join the domain for you,
#                              contrary to what happened with previous versions
#                              of epfl_sso! Instead, if epfl_sso detects that
#                              the Kerberos or Samba credentials are missing
#                              you will be directed to interactively use the
#                              "adjoin" script (provided by this module).
#
# $renew_domain_credentials::  Whether to periodically renew the
#                              Kerberos keytab entry and/or Samba
#                              machine account password. RECOMMENDED
#                              unless this machine is a clonable
#                              master that shares the same Active
#                              Directory computer object with a number
#                              of clones (and you don't care about the
#                              lack of perennity of that setup).
#                              Default is the same as $join_domain,
#                              and ignored if $join_domain is false.
#
# $sshd_gssapi_auth::          Set to true to allow inbound ssh access with
#                              Kerberos authentication. Default is the
#                              same as $join_domain, and ignored if
#                              $join_domain is false.
#
# $debug_gssd::                Turn extra debugging on in rpc.gssd if true
#
# $debug_sssd::                Turn extra debugging on in sssd if true
#
# === Actions:
#
# Unless otherwise stated, all actions are for the Linux platform only.
#
# * (Linux if either $auth_source or $directory_source is "AD", *and*
#   Mac OS X unconditionally) Set up client configuration for Active
#   Directory's Kerberos (krb5.conf) and LDAP (SSL certificates)
#
# * Install SSSD and configure it to access directory data (nsswitch)
#   and for authentication data (PAM) from either scoldap.epfl.ch or
#   Active Directory, depending on the respective settings of
#   $directory_source and $auth_source
#
# * Ensure that customarily used login shells at EPFL are installed,
#   and optionally set up an Access Control List (ACL) based on
#   pam_access (SSSD's similar feature is not used)
#
# * Configure sshd for inbound Kerberos authentication (unless
#   $sshd_gssapi_auth is explicitly set to false)
# 
class epfl_sso(
  $allowed_users_and_groups = undef,
  $manage_nsswitch_netgroup = true,
  $enable_mkhomedir = undef,
  $ad_automount_home = false,
  $auth_source = "AD",
  $directory_source = "scoldap",
  $needs_nscd = $::epfl_sso::private::params::needs_nscd,
  $ad_server = $epfl_sso::private::params::ad_server,
  $ad_server_base_dn = $epfl_sso::private::params::ad_server_base_dn,
  $realm = $epfl_sso::private::params::realm,
  $join_domain = false,
  $renew_domain_credentials = undef,
  $sshd_gssapi_auth = undef,
  $debug_gssd = undef,
  $debug_sssd = undef
) inherits epfl_sso::private::params {
  if ( (versioncmp($::puppetversion, '3') < 0) or
       (versioncmp($::puppetversion, '6') > 0) ) {
    fail("Need version 3.x thru 5.x of Puppet.")
  }

  assert_bool($manage_nsswitch_netgroup)
  assert_bool($join_domain)
  if ($renew_domain_credentials != undef) {
    assert_bool($renew_domain_credentials)
  }
  if ($sshd_gssapi_auth != undef) {
    assert_bool($sshd_gssapi_auth)
  }
  if ($allowed_users_and_groups != undef) {
    assert_string($allowed_users_and_groups)
  }

  if ($enable_mkhomedir != undef) {
    if ($enable_mkhomedir and $ad_automount_home) {
      fail('$enable_mkhomedir and $ad_automount_home are incompatible with each other!')
    }
    $_do_enable_mkhomedir = $enable_mkhomedir
  } else {
    $_do_enable_mkhomedir = ($directory_source != "AD")
  }

  if ($ad_automount_home and $directory_source != "AD") {
    fail('$ad_automount_home requires $directory_source == "AD"')
  }

  if (($join_domain == undef) and ($directory_source == "AD")) {
    warn("In order to be an Active Directory LDAP client, one must join the domain (obtain a Kerberos keytab). Consider passing the $join_domain parameter to the epfl_sso class")
  }

  case $::kernel {
    'Darwin': {
      if ($join_domain) {
        fail("Joining Active Directory domain on Mac OS X is not supported")
      }
      if ($enable_mkhomedir) {
        fail("mkhomedir is not supported on Mac OS X")
      }
      if ($ad_automount_home) {
        fail("Home automounts are not supported on Mac OS X")
      }
      class { "epfl_sso::private::ad":
        join_domain => false,
        ad_server   => $ad_server
      }
    }
    'Linux': {
      class { "epfl_sso::private::init_linux":
        allowed_users_and_groups => $allowed_users_and_groups,
        manage_nsswitch_netgroup => $manage_nsswitch_netgroup,
        enable_mkhomedir         => $_do_enable_mkhomedir,
        ad_automount_home        => $ad_automount_home,
        auth_source              => $auth_source,
        directory_source         => $directory_source,
        needs_nscd               => $needs_nscd,
        ad_server                => $ad_server,
        ad_server_base_dn        => $ad_server_base_dn,
        realm                    => $realm,
        join_domain              => $join_domain,
        renew_domain_credentials => pick($renew_domain_credentials, $join_domain),
        sshd_gssapi_auth         => pick($sshd_gssapi_auth, $join_domain),
        debug_gssd               => $debug_gssd,
        debug_sssd               => $debug_sssd
      }
    }
  }
}
