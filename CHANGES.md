# Version 0.6.x

- Explicitly configure /etc/samba/smb.conf for domain membership

- New "adjoin" script to drive all the msktutil business
   - adjoin join OU=foo,...   # Does the interactive part; kinit required prior
   - adjoin update
   - adjoin status            # Goes into /etc/cron.daily/renew-AD-credentials
