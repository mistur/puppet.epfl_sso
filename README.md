# puppet.epfl_sso
UNIX single sign-on using EPFL's LDAP and Kerberos servers

# Apply one-shot

[Install Puppet standalone](https://docs.puppetlabs.com/puppet/3.8/reference/pre_install.html#standalone-puppet) then, as *root*:

  1. <pre>puppet module install epflsti-epfl_sso</pre>
  3.  <pre>puppet apply -e "class { 'quirks': }"</pre>
  3.  <pre>puppet apply -e "class { 'epfl_sso': ...<i>See below</i>... }"</pre>
  4. If you are attempting to join the domain for the first time (i.e. `join_domain => true`; see below) this will stop mid-way with an error message that directs you to join the domain interactively. Here is how that could go:<pre>
     kinit AD123456
     /usr/local/sbin/adjoin join OU=iccluster,OU=IC</pre>
  5. Run the `puppet apply` command line from step 3 once more, this time to completion hopefully
  6. If home automounting was requested (i.e. `ad_automount_home => true`), [reboot](https://gitlab.com/epfl-sti/puppet.epfl_sso/issues/25)

# Apply as part of a Puppet server + agent deployment

Refer to [the Puppet documentation](https://puppet.com/docs/puppet/5.3/lang_classes.html#using-resource-like-declarations)

# Class Parameters (Examples)

💡 For the complete reference, see the comments at the top of [`init.pp`](https://gitlab.com/epfl-sti/puppet.epfl_sso/blob/master/manifests/init.pp)

“Bells and whistles” configuration, if you are in command of a suitably powerful [ADsciper account](https://winad.epfl.ch/core/printable.asp?ID=25):

<pre>puppet apply -e "class { 'epfl_sso':
          allowed_users_and_groups => 'user1 user2 (group1) (group2)',
          auth_source => 'AD',
          directory_source => 'AD',
          join_domain => true,
          ad_automount_home => true
      }"
</pre>

Poor man's “computer-object-less” configuration for unaccredited
administrators: Kerberos outbound-only, no roaming `/home`, but still
the same UIDs, passwords and (mostly) same groups as everyone else:

<pre>puppet apply -e "class { 'epfl_sso':
          allowed_users_and_groups => 'user1 user2 (group1) (group2)',
          auth_source => 'AD',
          directory_source => 'scoldap',
          enable_mkhomedir => true
      }"
</pre>

<dl>
    <dt><code>allowed_users_and_groups</code> (<b>recommended</b>)</dt>
    <dd>
    The list of users and groups that will be permitted access. <code>user1</code>, <code>user2</code> are GASPAR usernames (or local accounts created manually or outside of <code>epfl_sso</code>); <code>group1</code> and <code>group2</code> are [EPFL groups](https://groups.epfl.ch) which are visible in whichever <code>directory_source</code> you choose (see below)
    </dd>
    <dt><code>join_domain</code></dt>
    <dd>Whether to enroll into the domain. <b><code>epfl_sso</code> will not actually do that for you</b>, since that requires authentication (typically with your so-called “ADsciper” account, which you should have created as a “responsable informatique”) and cannot be done interactively. The module will only check / renew the credentials, or direct you with a useful error message and wrapper script if they are invalid or missing.</dd>
    <dt><code>auth_source</code></dt>
    <dd>Where to pick authentication from (password / Kerberos ticket validation) - Basically, how to configure <code>pam</code> and <code>sssd</code>. Permitted values are <code>"AD"</code> and <code>"scoldap"</code></dd>
    <dt><code>directory_source</code></dt>
    <dd>Where to pick group information from. Permitted values are <code>"AD"</code> and <code>"scoldap"</code>. Note that <code>"AD"</code> requires membership in the domain (<code>join_domain</code>) to work, but has strictly more groups (i.e. Active Directory groups, not just groups out of <a href="https://groups.epfl.ch">groups.epfl.ch</a>).</dd>
    <dt><code>ad_automount_home</code> (new!)</dt>
    <dd>Whether to automount user directories from their so-called “MyNAS” shares over Kerberized NFSv4. Requires <code>join_domain</code>.</dd>
    <dt><code>enable_mkhomedir</code></dt>
    <dd>Auto-create user directories in a <code>/home</code> on a local disk. For use when <code>join_domain</code> is not an option (due to a lack of a suitably authorized ADsciper account, for instance).</dd>
</dl>

## Development

To work off the latest ("master") version of `epfl_sso`:

  1. Be sure to remove previous version: `puppet module uninstall epflsti-epfl_sso` (add `--ignore-changes` if needed)
  1. Go in the puppet folder: `cd /etc/puppet/code/modules` (your mileage may vary on different distributions)
  1. Remove `epfl_sso` (but it should have been done at step 1)
  1. Clone the repo here: `git clone https://gitlab.com/epfl-sti/puppet.epfl_sso.git epfl_sso`
